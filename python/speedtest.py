#import nfc

import RPi.GPIO as GPIO
import logging
import sys
import datetime
import smbus
import time
import pygame
from random import randint
from time import sleep
import threading
#import multiprocessing
from Queue import Queue
import alsaaudio
from ConfigParser import SafeConfigParser
import peewee
from peewee import *
from Queue import Queue

# Settings
conf = SafeConfigParser()
conf.read('/home/pi/Speedtest/python/speedtest.cfg')
path = conf.get('main', 'path')
unPressedLimit = conf.getint('main', 'unpressedButtonsLimit')
allowSkipScores = conf.getboolean('main', 'allowSkipScores')
skipScore = conf.getint('main', 'skipScore')
maxRepeatedButtons = conf.getint('main', 'maxRepeatedButtons')

# Creating logger
logger = logging.getLogger("speedtest")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
fh = logging.FileHandler(path + 'speedtest.txt')
logger.addHandler(fh)

demux0 = 4
demux1 = 17
demux2 = 27

statusRed = 14
statusGreen = 15
statusBlue = 22

LD1 = 23
LD2 = 24
LD3 = 25
LD4 = 8

A = 12
B = 16
C = 20
D = 21

buttonLED1 = 10
buttonLED2 = 9
buttonLED3 = 11
buttonLED4 = 5
buttonLEDReset = 7

button0 = 6
button1 = 13
button2 = 19
button3 = 26
buttonReset = 18

class Result(Model):
	id = PrimaryKeyField(primary_key=True)
	score = IntegerField()
	date = DateTimeField(default=datetime.datetime.now)
	name = CharField()
	class Meta:
		database = MySQLDatabase('speedtest', host=conf.get('main', 'sqlAddress'), port=conf.getint('main', 'sqlPort'), user=conf.get('main', 'sqlUser'),passwd=conf.get('main', 'sqlPassword'))

class Color:
	red = statusRed
	green = statusGreen
	blue = statusBlue
	all = 100

class FuncThread(threading.Thread):
	def __init__(self, target, *args):
		self._running = True
		self._target = target
		self._args = args
		threading.Thread.__init__(self)
	
	def stop(self):
		logger.info("Stop received")
		self._running = False
	
	def run(self):
		self._target(self, *self._args)

def Init():
	logger.info("Initializing...")
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(demux0, GPIO.OUT)
	GPIO.setup(demux1, GPIO.OUT)
	GPIO.setup(demux2, GPIO.OUT)
	
	GPIO.setup(statusRed, GPIO.OUT)
	GPIO.setup(statusGreen, GPIO.OUT)
	GPIO.setup(statusBlue, GPIO.OUT)
	
	GPIO.setup(LD1, GPIO.OUT)
	GPIO.setup(LD2, GPIO.OUT)
	GPIO.setup(LD3, GPIO.OUT)
	GPIO.setup(LD4, GPIO.OUT)
	
	GPIO.setup(A, GPIO.OUT)
	GPIO.setup(B, GPIO.OUT)
	GPIO.setup(C, GPIO.OUT)
	GPIO.setup(D, GPIO.OUT)
	
	GPIO.setup(buttonLED1, GPIO.OUT)
	GPIO.setup(buttonLED2, GPIO.OUT)
	GPIO.setup(buttonLED3, GPIO.OUT)
	GPIO.setup(buttonLED4, GPIO.OUT)
	GPIO.setup(buttonLEDReset, GPIO.OUT)
	
	GPIO.setup(button0, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
	GPIO.setup(button1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
	GPIO.setup(button2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
	GPIO.setup(button3, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
	GPIO.setup(buttonReset, GPIO.IN)
	
	# Turning off all outputs
	GPIO.output(demux0, False)
	GPIO.output(demux1, False)
	GPIO.output(demux2, False)
	
	GPIO.output(statusRed, False)
	GPIO.output(statusGreen, False)
	GPIO.output(statusBlue, False)
	
	# LDs to true, so that the numbers will be change to zero
	GPIO.output(LD1, True)
	GPIO.output(LD2, True)
	GPIO.output(LD3, True)
	GPIO.output(LD4, True)
	
	GPIO.output(A, False)
	GPIO.output(B, False)
	GPIO.output(C, False)
	GPIO.output(D, False)
	
	GPIO.output(LD1, False)
	GPIO.output(LD2, False)
	GPIO.output(LD3, False)
	GPIO.output(LD4, False)
	
	GPIO.output(buttonLED1, False)
	GPIO.output(buttonLED2, False)
	GPIO.output(buttonLED3, False)
	GPIO.output(buttonLED4, False)
	GPIO.output(buttonLEDReset, False)
	
	# Sounds
	pygame.mixer.pre_init(44100, -16, 1, 512)
	pygame.init()
	pygame.mixer.init()
	
	# Setting events for button presses during the game
	GPIO.add_event_detect(button0, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	GPIO.add_event_detect(button1, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	GPIO.add_event_detect(button2, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	GPIO.add_event_detect(button3, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	
	logger.info("Checking database")
	# Checking that database exists
	try:
		if (Result.table_exists() == False):
			logger.info("Table doesn't exist, creating...")
			Result.create_table()
	except Exception as e:
		logger.info("Couldn't check table: " + str(e))
	
	logger.info("Initialized")

def CloseLED(LED):
	# The led can be closed by setting a binary value larger than 1001
	# Setting value 1111
	GPIO.output(A, 1)
	GPIO.output(B, 1)
	GPIO.output(C, 1)
	GPIO.output(D, 1)
	if (LED == 1):
		# Turning LD momentary on
		GPIO.output(LD1, 1)
		#Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD momentary on
		GPIO.output(LD2, 1)
		#Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD momentary on
		GPIO.output(LD3, 1)
		#Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 4):
		# Turning LD momentary on
		GPIO.output(LD4, 1)
		#Turning LD back off
		GPIO.output(LD4, 0)

# Converts decimal number to binary number
def IntToBinArray(value):
	returnValue = [0, 0, 0, 0]
	
	if (value > 9):
		return returnValue
	
	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 4 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr
	
	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i])
	
	return returnValue

# Set's a specific LED's number
def SetLEDValue(LED, number):
	# Converting number to binary array
	binArray = IntToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		logger.error("Error, number is not 4-bit")
		return 0
	
	GPIO.output(D, binArray[0]) # D
	GPIO.output(C, binArray[1]) # C
	GPIO.output(B, binArray[2]) # B
	GPIO.output(A, binArray[3]) # A
	
	if (LED == 0):
		# Turning LD on
		GPIO.output(LD1, 1)
		# Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 1):
		# Turning LD on
		GPIO.output(LD2, 1)
		# Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 2):
		# Turning LD on
		GPIO.output(LD3, 1)
		# Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 3):
		# Turning LD on
		GPIO.output(LD4, 1)
		# Turning LD back off
		GPIO.output(LD4, 0)

def showLevelLED(endScore):
	level = 0
	if (endScore == 0):
		level = 0 
	elif (endScore >= conf.getint('main', 'level1') and endScore < conf.getint('main', 'level2')):
		level = 1 # Myyja
	elif (endScore >= conf.getint('main', 'level2') and endScore < conf.getint('main', 'level3')):
		level = 2 # Lapsi
	elif (endScore >= conf.getint('main', 'level3') and endScore < conf.getint('main', 'level4')):
		level = 3 # PP
	elif (endScore >= conf.getint('main', 'level4') and endScore < conf.getint('main', 'level5')):
		level = 4 # PRO
	elif (endScore >= conf.getint('main', 'level5') and endScore < conf.getint('main', 'level6')):
		level = 5 # Koodari
	elif (endScore >= conf.getint('main', 'level6') and endScore < conf.getint('main', 'level7')):
		level = 6 # Guru
	elif (endScore >= conf.getint('main', 'level7')):
		level = 7 # Nero
	
	setLevelLED(level)
	
	# Game ended and end score is known, saving to the database
	if (level > 0):
		try:
			result = Result(score = endScore)
			result.save()
		except Exception as e:
			logger.info("Could not save score to database: " + str(e))

def setScoreBoardNumber(number):
	numberStr = str(number)
	# Adding zeros
	if (len(numberStr) < 4):
		zeroAmount = 4 - len(numberStr)
		for i in range(zeroAmount):
			numberStr = "0" + numberStr
	for i in range(4):
		SetLEDValue(i, int(numberStr[i]))

def setLevelLED(number):
	binArray = IntToBinArray(number)
	GPIO.output(demux2, binArray[1])
	GPIO.output(demux1, binArray[2])
	GPIO.output(demux0, binArray[3])

def setStatusLED(color, state):
	if (color == Color.all):
		GPIO.output(statusRed, state)
		GPIO.output(statusGreen, state)
		GPIO.output(statusBlue, state)
	else:
		GPIO.output(color, state)

def SetButtonLED(LED, ON):
	if (LED == 0):
		GPIO.output(buttonLED1, ON)
	elif (LED == 1):
		GPIO.output(buttonLED2, ON)
	elif (LED == 2):
		GPIO.output(buttonLED3, ON)
	elif (LED == 3):
		GPIO.output(buttonLED4, ON)

def addScore():
	global gameScore
	
	# Adding to the score
	gameScore += 1
	
	# Displaying the score
	setScoreBoardNumber(gameScore)

def buttonRoulette(thread, beeps, startSpeed, acceleration):
	global gameRunning
	#global selectedButtons
	global selectedButtonsQueue
	global gameOverSound
	global gameScore
	speed = startSpeed
	buttonBCM = [button0, button1, button2, button3]
	
	previousButton = -1
	buttonRepeated = 0
	
	while gameRunning:
	
		# If too many missed buttons
		unPressed = getUnpressedButtonsAmount()
		if (unPressed > unPressedLimit):
			gameRunning = False
			logger.info("Unpressed: " + str(unPressed))
			gameOverSound.play()
			showLevelLED(gameScore)
			break
		
		while True:
			selectedButton = randint(0, 3)
			if (previousButton == selectedButton):
				# Button was same as previously, adding to repeated amount
				buttonRepeated += 1
			else:
				buttonRepeated = 0
			
			if (previousButton != selectedButton or buttonRepeated <= maxRepeatedButtons):
				# Button was not repeated or it's within the max repeated amount, no new button is needed to be selected
				break
			else:
				print("Button was repeated, selecting new")
		
		previousButton = selectedButton
		
		
		selectedButtonsQueue.put(selectedButton)
		
		# Selecting the right beep sound
		beep = beeps[selectedButton]
		SetButtonLED(selectedButton, True)
		beep.play()
		
		# Checking if game is still running, exit if not
		if (gameRunning == False):
			# Turning light and beep off
			beep.stop()
			SetButtonLED(selectedButton, False)
			break
		else:
			# Keeping beep and light on
			sleep(speed + 0.1)
		
		# Closing light
		SetButtonLED(selectedButton, False)
		beep.stop()
		
		# Checking if game is still running
		if (gameRunning == False):
			break
		else:
			# Keeping light off, until next button is to be lit
			sleep(speed)
		
		#Accelerating speed for next round
		if (speed - acceleration > 0):
			speed -= acceleration

def getNextButtonToPress():
	global selectedButtonsQueue
	if (selectedButtonsQueue.qsize() == 0):
		return None
	else:
		# Returns the oldest value (button that needs to be pressed) and removes it from the queue
		return selectedButtonsQueue.get()

def getUnpressedButtonsAmount():
	#global selectedButtons
	global selectedButtonsQueue
	return selectedButtonsQueue.qsize()

def convertButtonGPIONumber(gpio):
	if (gpio == button0):
		return 0
	elif (gpio == button1):
		return 1
	elif (gpio == button2):
		return 2
	elif (gpio == button3):
		return 3
	else:
		return -1

def gameButtonPressed(buttonPressedGpio):
	
	global gameRunning
	global gameOverSound
	global gameScore
	
	# Used to prevent this function from being run twice with just one press
	#global currentButtonPressed
	#global currentButtonDown
	
	# If this is the first time calling this callback
	#if (currentButtonDown == False):
	#currentButtonDown = True
	#currentButtonPressed = buttonPressedGpio
	
	# Making sure that game is running and that button was pressed down, not lifted up
	if (GPIO.input(buttonPressedGpio) == False):
		buttonPressed = convertButtonGPIONumber(buttonPressedGpio)
		
		logger.info("Pressed button: " + str(buttonPressed))
		print("Pressed button: " + str(buttonPressed))
		if (gameRunning):
		
			buttonPressed = convertButtonGPIONumber(buttonPressedGpio)
			
			logger.info("Pressed button: " + str(buttonPressed))
			print("Pressed button: " + str(buttonPressed))
			
			# Checking what is the next button we need to pressed
			nextButtonToPress = getNextButtonToPress()
			# If player pressed the wrong button
			if (nextButtonToPress == None or nextButtonToPress != buttonPressed):
				reason = "None" if nextButtonToPress == None else str(nextButtonToPress)
				logger.info("Player pressed: " + str(buttonPressed) + " which was wrong. Should have been: " + reason)
				gameRunning = False
				gameOverSound.play()
				showLevelLED(gameScore)
			else:
				addScore()
	# If this is the second time calling this callback for this specific button, and the button is going up
	#elif(GPIO.input(buttonPressedGpio) == True):
	#	currentButtonDown = False
	#	currentButtonPressed = -1

def game(thread, startScore):
	global gameRunning
	global selectedButtonsQueue
	global gameScore
	global gameOverSound
	
	# Used in gameButtonPressed to prevent button callback being run twice
	#global currentButtonPressed
	#currentButtonPressed = None
	#global currentButtonDown
	#currentButtonDown = False
	
	selectedButtonsQueue = Queue()
	gameRunning = True
	gameScore = startScore
	setScoreBoardNumber(gameScore)
	showLevelLED(0)
	gameOverSound = pygame.mixer.Sound(path + "sounds/gameover0.wav")
	
	beep0 = pygame.mixer.Sound(path + "sounds/beep0.wav")
	beep1 = pygame.mixer.Sound(path + "sounds/beep1.wav")
	beep2 = pygame.mixer.Sound(path + "sounds/beep2.wav")
	beep3 = pygame.mixer.Sound(path + "sounds/beep3.wav")
	
	beeps = [beep0, beep1, beep2, beep3]
	
	# Calculating start speed
	startSpeed = conf.getfloat('main', 'startSpeed')
	acceleration = conf.getfloat('main', 'acceleration')
	startSpeed -= acceleration * startScore
	if (startSpeed < 0):
		startSpeed = 0
	
	# Sleeping for a random time, so that game doesn't start immediately
	randStartTime = randint(1, 3)
	sleep(randStartTime)
	
	buttonRouletteThread = FuncThread(buttonRoulette, beeps, startSpeed, acceleration)
	buttonRouletteThread.start()
	
	while(gameRunning and thread._running):
		sleep(1)

class Volume(object):
	def __init__(self, volume, mixerCard, mixerControl):
		self._value = volume
		self._bindings = []
		self._mixer = alsaaudio.Mixer(cardindex=mixerCard, control=mixerControl)
	
	@property
	def value(self, volume):
		return self._value
	
	@value.setter
	def value(self, newVolume):
		if (newVolume != self._value):
			
			self._value = newVolume
			for callback in self._bindings:
				callback(self._value, self._mixer)
	
	def bind(self, callback):
		self._bindings.append(callback)

def volumeChanged(newVolume, mixer):
	# Setting volume through alsaaudio
	mixer.setvolume(newVolume)
	
	# Showing new volume
	logger.info("New volume: " + str(newVolume))

def volumeControl(thread):
	logger.info("ThreadRunning: " + str(thread._running))
	address = 0x48
	A0 = 0x40
	A1 = 0x41
	A2 = 0x42
	A3 = 0x43
	bus = smbus.SMBus(1)
	# Getting starting value for the volume
	bus.write_byte(address,A0)
	value = bus.read_byte(address)
	startingVolume = 0
	if (conf.getboolean('main', 'reverseVolume')):
		startingVolume = 100 - int(value / 255.0 * 100.0)
	else:
		startingVolume = int(value / 255.0 * 100.0)
	
	volume = Volume(startingVolume, 1, "Speaker")
	volume.bind(volumeChanged)
	logger.info("Starting volume thread")
	
	while thread._running:
		bus.write_byte(address,A0)
		value = bus.read_byte(address)
		if (conf.getboolean('main', 'reverseVolume')):
			volume.value = 100 - int(value / 255.0 * 100.0)
		else:
			volume.value = int(value / 255.0 * 100.0)
		time.sleep(0.5)

def startButtonPressed(button):
	global gameRunning
	
	logger.info("Start button pressed")
	if (gameRunning == False):
		
		# If allowed to skip scores and last button is pressed at the same time
		startScore = 0;
		if (allowSkipScores and GPIO.input(button3) == False):
			startScore = skipScore
		
		gameThread = FuncThread(game, startScore)
		gameThread.start()

# Not used
def nfcReader(thread):
	logger.info('Version: ', nfc.__version__)
	context = nfc.init()
	pnd = nfc.open(context)
	if pnd is None:
		logger.info("ERROR: Could not open nfc")
	
	if nfc.initiator_init(pnd) < 0:
		nfc.perror(pnd, "nfc_initiator_init")
		logger.info('ERROR: Unable to init NFC device.')
		sys.exit(1)
	
	logger.info('NFC reader: %s opened' % nfc.device_get_name(pnd))
	
	nmMifare = nfc.modulation()
	nmMifare.nmt = nfc.NMT_ISO14443A
	nmMifare.nbr = nfc.NBR_106
	
	nt = nfc.target()
	ret = nfc.initiator_select_passive_target(pnd, nmMifare, 0, 0, nt)
	
	logger.info('The following (NFC) ISO14443A tag was found:')
	logger.info('ATQA (SENS_RES): ')
	nfc.print_hex(nt.nti.nai.abtAtqa, 2)
	id = 1
	if nt.nti.nai.abtUid[0] == 8:
		id = 3
	logger.info('UID (NFCID%d): ' % id)
	nfc.print_hex(nt.nti.nai.abtUid, nt.nti.nai.szUidLen)
	logger.info('SAK (SEL_RES): ')
	logger.info(nt.nti.nai.btSak)
	if nt.nti.nai.szAtsLen:
		logger.info('ATS (ATR): ')
		nfc.print_hex(nt.nti.nai.abtAts, nt.nti.nai.szAtsLen)

	nfc.close(pnd)
	nfc.exit(context)

def statusLEDController(thread):
	# Closing status LED
	setStatusLED(Color.all, False)
	global gameRunning
	
	currentColor = None
	
	while thread._running:
		if (gameRunning and currentColor != Color.blue):
			setStatusLED(Color.all, False)
			setStatusLED(Color.blue, True)
			currentColor = Color.blue
		elif (gameRunning == False and currentColor != Color.green):
			setStatusLED(Color.all, False)
			setStatusLED(Color.green, True)
			currentColor = Color.green
		
		sleep(0.5)

def main(argv):
	
	success = True
	threads = []
	global gameRunning
	global init
	gameRunning = False
	try:
		Init()
		
		statusLEDControllerThread = FuncThread(statusLEDController)
		statusLEDControllerThread.start()
		
		volumeControlThread = FuncThread(volumeControl)
		volumeControlThread.start()
		
		#Nfc disabled, for now. Didn't work well in a thread
		#nfcThread = FuncThread(nfcReader)
		#nfcThread.start()
		
		# Binding event for pressing game reset (start) button
		GPIO.add_event_detect(buttonReset, GPIO.RISING, callback=startButtonPressed, bouncetime=50)
		
		threads.append(volumeControlThread)
		threads.append(statusLEDControllerThread)
		
		setStatusLED(Color.all, False)
		setStatusLED(Color.green, True)
		
		while True:
			sleep(10)
		
	except KeyboardInterrupt:
		# CTRL-C pressed, killing all threading
		logger.info("Sending kill to threads")
		for t in threads:
			t.stop()
		gameRunning = False
	finally:
		logger.info("Program shutting down...")
		GPIO.cleanup()
		return success

if __name__ == "__main__": # Checking if this is the main program, not an imported module
	while True:
		returnValue = main(sys.argv)
		if (returnValue == True): # Exiting only if main didn't stop due to an error
			logger.info("Clean exit, shutting program down...")
			break
		else: # Main crashed due to an error. This is propably because an I/O error. This error is usually fixed by itself. Waiting 1 second and then starting main again
			logger.error("Error, restarting in 1 second...")
			time.sleep(1)
