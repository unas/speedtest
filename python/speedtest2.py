import RPi.GPIO as GPIO
import logging
import sys
import datetime
import smbus
import time
import pygame
from random import randint
from time import sleep
import threading
#import multiprocessing
from Queue import Queue
import alsaaudio
from ConfigParser import SafeConfigParser
import peewee
from peewee import *
from Queue import Queue

conf = SafeConfigParser()
conf.read('/home/pi/Speedtest/python/speedtest.cfg')

path = conf.get('main', 'path')
unPressedLimit = conf.getint('main', 'unpressedButtonsLimit')

# Creating logger
logger = logging.getLogger("speedtest")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
fh = logging.FileHandler(path + 'speedtest.txt')
logger.addHandler(fh)

demux0 = 4
demux1 = 17
demux2 = 27

statusRed = 14
statusGreen = 15
statusBlue = 22

LD1 = 23
LD2 = 24
LD3 = 25
LD4 = 8

A = 12
B = 16
C = 20
D = 21

buttonLED1 = 10
buttonLED2 = 9
buttonLED3 = 11
buttonLED4 = 5
buttonLEDReset = 7

button0 = 6
button1 = 13
button2 = 19
button3 = 26
buttonReset = 18

class Result(Model):
	id = PrimaryKeyField(primary_key=True)
	score = IntegerField()
	date = DateTimeField(default=datetime.datetime.now)
	name = CharField()
	class Meta:
		database = MySQLDatabase('speedtest', host=conf.get('main', 'sqlAddress'), port=conf.getint('main', 'sqlPort'), user=conf.get('main', 'sqlUser'),passwd=conf.get('main', 'sqlPassword'))

class Color:
	red = statusRed
	green = statusGreen
	blue = statusBlue
	all = 100

class FuncThread(threading.Thread):
	def __init__(self, target, *args):
		self._running = True
		self._target = target
		self._args = args
		threading.Thread.__init__(self)
	
	def stop(self):
		logger.info("Stop received")
		self._running = False
	
	def run(self):
		self._target(self, *self._args)

def Init():
	logger.info("Initializing...")
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(demux0, GPIO.OUT)
	GPIO.setup(demux1, GPIO.OUT)
	GPIO.setup(demux2, GPIO.OUT)
	
	GPIO.setup(statusRed, GPIO.OUT)
	GPIO.setup(statusGreen, GPIO.OUT)
	GPIO.setup(statusBlue, GPIO.OUT)
	
	GPIO.setup(LD1, GPIO.OUT)
	GPIO.setup(LD2, GPIO.OUT)
	GPIO.setup(LD3, GPIO.OUT)
	GPIO.setup(LD4, GPIO.OUT)
	
	GPIO.setup(A, GPIO.OUT)
	GPIO.setup(B, GPIO.OUT)
	GPIO.setup(C, GPIO.OUT)
	GPIO.setup(D, GPIO.OUT)
	
	GPIO.setup(buttonLED1, GPIO.OUT)
	GPIO.setup(buttonLED2, GPIO.OUT)
	GPIO.setup(buttonLED3, GPIO.OUT)
	GPIO.setup(buttonLED4, GPIO.OUT)
	GPIO.setup(buttonLEDReset, GPIO.OUT)
	
	GPIO.setup(button0, GPIO.IN)
	GPIO.setup(button1, GPIO.IN)
	GPIO.setup(button2, GPIO.IN)
	GPIO.setup(button3, GPIO.IN)
	GPIO.setup(buttonReset, GPIO.IN)
	
	# Turning off all outputs
	GPIO.output(demux0, False)
	GPIO.output(demux1, False)
	GPIO.output(demux2, False)
	
	GPIO.output(statusRed, False)
	GPIO.output(statusGreen, False)
	GPIO.output(statusBlue, False)
	
	# LDs to true, so that the numbers will be change to zero
	GPIO.output(LD1, True)
	GPIO.output(LD2, True)
	GPIO.output(LD3, True)
	GPIO.output(LD4, True)
	
	GPIO.output(A, False)
	GPIO.output(B, False)
	GPIO.output(C, False)
	GPIO.output(D, False)
	
	GPIO.output(LD1, False)
	GPIO.output(LD2, False)
	GPIO.output(LD3, False)
	GPIO.output(LD4, False)
	
	GPIO.output(buttonLED1, False)
	GPIO.output(buttonLED2, False)
	GPIO.output(buttonLED3, False)
	GPIO.output(buttonLED4, False)
	GPIO.output(buttonLEDReset, False)
	
	# Sounds
	pygame.mixer.pre_init(44100, -16, 1, 512)
	pygame.init()
	pygame.mixer.init()
	
	# Setting events for button presses during the game
	GPIO.add_event_detect(button0, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	GPIO.add_event_detect(button1, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	GPIO.add_event_detect(button2, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	GPIO.add_event_detect(button3, GPIO.RISING, callback=gameButtonPressed, bouncetime=200)
	
	logger.info("Checking database")
	# Checking that database exists
	try:
		if (Result.table_exists() == False):
			logger.info("Table doesn't exist, creating...")
			Result.create_table()
	except Exception as e:
		logger.info("Couldn't check table: " + str(e))
	
	logger.info("Initialized")

def CloseLED(LED):
	# The led can be closed by setting a binary value larger than 1001
	# Setting value 1111
	GPIO.output(A, 1)
	GPIO.output(B, 1)
	GPIO.output(C, 1)
	GPIO.output(D, 1)
	if (LED == 1):
		# Turning LD momentary on
		GPIO.output(LD1, 1)
		#Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 2):
		# Turning LD momentary on
		GPIO.output(LD2, 1)
		#Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 3):
		# Turning LD momentary on
		GPIO.output(LD3, 1)
		#Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 4):
		# Turning LD momentary on
		GPIO.output(LD4, 1)
		#Turning LD back off
		GPIO.output(LD4, 0)

# Converts decimal number to binary number
def IntToBinArray(value):
	returnValue = [0, 0, 0, 0]
	
	if (value > 9):
		return returnValue
	
	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 4 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr
	
	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i])
	
	return returnValue

# Set's a specific LED's number
def SetLEDValue(LED, number):
	# Converting number to binary array
	binArray = IntToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		logger.error("Error, number is not 4-bit")
		return 0
	
	GPIO.output(D, binArray[0]) # D
	GPIO.output(C, binArray[1]) # C
	GPIO.output(B, binArray[2]) # B
	GPIO.output(A, binArray[3]) # A
	
	if (LED == 0):
		# Turning LD on
		GPIO.output(LD1, 1)
		# Turning LD back off
		GPIO.output(LD1, 0)
	elif (LED == 1):
		# Turning LD on
		GPIO.output(LD2, 1)
		# Turning LD back off
		GPIO.output(LD2, 0)
	elif (LED == 2):
		# Turning LD on
		GPIO.output(LD3, 1)
		# Turning LD back off
		GPIO.output(LD3, 0)
	elif (LED == 3):
		# Turning LD on
		GPIO.output(LD4, 1)
		# Turning LD back off
		GPIO.output(LD4, 0)

def showLevelLED(endScore):
	level = 0
	if (endScore == 0):
		level = 0 
	elif (endScore >= conf.getint('main', 'level1') and endScore < conf.getint('main', 'level2')):
		level = 1 # Myyja
	elif (endScore >= conf.getint('main', 'level2') and endScore < conf.getint('main', 'level3')):
		level = 2 # Lapsi
	elif (endScore >= conf.getint('main', 'level3') and endScore < conf.getint('main', 'level4')):
		level = 3 # PP
	elif (endScore >= conf.getint('main', 'level4') and endScore < conf.getint('main', 'level5')):
		level = 4 # PRO
	elif (endScore >= conf.getint('main', 'level5') and endScore < conf.getint('main', 'level6')):
		level = 5 # Koodari
	elif (endScore >= conf.getint('main', 'level6') and endScore < conf.getint('main', 'level7')):
		level = 6 # Guru
	elif (endScore >= conf.getint('main', 'level7')):
		level = 7 # Nero
	
	setLevelLED(level)
	
	# Game ended and end score is known, saving to the database
	if (level > 0):
		try:
			result = Result(score = endScore)
			result.save()
		except Exception as e:
			logger.info("Could not save score to database: " + str(e))

def setScoreBoardNumber(number):
	numberStr = str(number)
	# Adding zeros
	if (len(numberStr) < 4):
		zeroAmount = 4 - len(numberStr)
		for i in range(zeroAmount):
			numberStr = "0" + numberStr
	for i in range(4):
		SetLEDValue(i, int(numberStr[i]))

def setLevelLED(number):
	binArray = IntToBinArray(number)
	GPIO.output(demux2, binArray[1])
	GPIO.output(demux1, binArray[2])
	GPIO.output(demux0, binArray[3])

def setStatusLED(color, state):
	if (color == Color.all):
		GPIO.output(statusRed, state)
		GPIO.output(statusGreen, state)
		GPIO.output(statusBlue, state)
	else:
		GPIO.output(color, state)

def SetButtonLED(LED, ON):
	if (LED == 0):
		GPIO.output(buttonLED1, ON)
	elif (LED == 1):
		GPIO.output(buttonLED2, ON)
	elif (LED == 2):
		GPIO.output(buttonLED3, ON)
	elif (LED == 3):
		GPIO.output(buttonLED4, ON)

def addScore():
	global gameScore
	
	# Adding to the score
	gameScore += 1
	
	# Displaying the score
	setScoreBoardNumber(gameScore)

class Volume(object):
	def __init__(self, volume, mixerCard, mixerControl):
		self._value = volume
		self._bindings = []
		self._mixer = alsaaudio.Mixer(cardindex=mixerCard, control=mixerControl)
	
	@property
	def value(self, volume):
		return self._value
	
	@value.setter
	def value(self, newVolume):
		if (newVolume != self._value):
			
			self._value = newVolume
			for callback in self._bindings:
				callback(self._value, self._mixer)
	
	def bind(self, callback):
		self._bindings.append(callback)

def volumeChanged(newVolume, mixer):
	# Setting volume through alsaaudio
	mixer.setvolume(newVolume)
	
	# Showing new volume
	logger.info("New volume: " + str(newVolume))

def volumeControl(thread):
	logger.info("ThreadRunning: " + str(thread._running))
	address = 0x48
	A0 = 0x40
	A1 = 0x41
	A2 = 0x42
	A3 = 0x43
	bus = smbus.SMBus(1)
	# Getting starting value for the volume
	bus.write_byte(address,A0)
	value = bus.read_byte(address)
	startingVolume = 0
	if (conf.getboolean('main', 'reverseVolume')):
		startingVolume = 100 - int(value / 255.0 * 100.0)
	else:
		startingVolume = int(value / 255.0 * 100.0)
	
	volume = Volume(startingVolume, 1, "Speaker")
	volume.bind(volumeChanged)
	logger.info("Starting volume thread")
	
	while thread._running:
		bus.write_byte(address,A0)
		value = bus.read_byte(address)
		if (conf.getboolean('main', 'reverseVolume')):
			volume.value = 100 - int(value / 255.0 * 100.0)
		else:
			volume.value = int(value / 255.0 * 100.0)
		time.sleep(0.5)

def startButtonPressed(button):
	global gameRunning
	
	logger.info("Start button pressed")
	if (gameRunning == False):
		gameThread = FuncThread(game)
		gameThread.start()

def game(thread):
	global gameRunning
	global selectedButtonsQueue
	global gameScore
	global gameOverSound
	
	selectedButtonsQueue = Queue()
	gameRunning = True
	gameScore = 0

def main(argv):
	
	success = True
	threads = []
	global gameRunning
	global init
	gameRunning = False
	try:
		Init()
		
		statusLEDControllerThread = FuncThread(statusLEDController)
		statusLEDControllerThread.start()
		
		volumeControlThread = FuncThread(volumeControl)
		volumeControlThread.start()
		
		# Binding event for pressing game reset (start) button
		GPIO.add_event_detect(buttonReset, GPIO.RISING, callback=startButtonPressed, bouncetime=50)
		
		threads.append(volumeControlThread)
		threads.append(statusLEDControllerThread)
		
		setStatusLED(Color.all, False)
		setStatusLED(Color.green, True)
		
		while True:
			sleep(10)
		
	except KeyboardInterrupt:
		# CTRL-C pressed, killing all threading
		logger.info("Sending kill to threads")
		for t in threads:
			t.stop()
		gameRunning = False
	finally:
		logger.info("Program shutting down...")
		GPIO.cleanup()
		return success

if __name__ == "__main__": # Checking if this is the main program, not an imported module
	while True:
		returnValue = main(sys.argv)
		if (returnValue == True): # Exiting only if main didn't stop due to an error
			logger.info("Clean exit, shutting program down...")
			break
		else: # Main crashed due to an error. This is propably because an I/O error. This error is usually fixed by itself. Waiting 1 second and then starting main again
			logger.error("Error, restarting in 1 second...")
			time.sleep(1)
