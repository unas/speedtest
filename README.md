# README #

Speed test game. Contains 3D-models, gerberfiles and code for raspberry pi.


### Install ###

- sudo pip install peewee
- sudo apt-get install python-dev libmysqlclient-dev
- sudo pip install MySQL-python
- sudo apt-get install python-smbus
- sudo apt-get install python-alsaaudio # find you devices mixer with "amixer -c 1", e.g. Speaker
- sudo apt-get install python-pygame
- sudo apt-get install python-pip
- sudo pip install pymysql

- sudo vim /usr/share/alsa/alsa.conf
	- Change the following, where 1 is your desired sound card
	defaults.ctl.card 1
	defaults.pcm.card 1

- sudo vim /etc/asound.conf
	pcm.!default {
		type hw
		card 1
	}

	ctl.!default {
		type hw
		card 1
	}

- https://blog.stigok.com/post/setting-up-a-pn532-nfc-module-on-a-raspberry-pi-using-i2c
- sudo apt install i2c-tools
- sudo apt install libnfc5 libnfc-bin libnfc-examples
	

#### NFC ####
- sudo vim /etc/nfc/libnfc.conf
	add:
	device.name = "PN532 over I2C"
	device.connstring = "pn532_i2c:/dev/i2c-1"
- sudo apt-get install cmake
- sudo apt-get install swig
- https://github.com/xantares/nfc-bindings
- git clone https://github.com/xantares/nfc-bindings.git
- cd nfc-bindings
- cmake -DCMAKE_INSTALL_PREFIX=~/.local .
- make install